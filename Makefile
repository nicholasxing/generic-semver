TARGET_NAME=autoversion.go

BINARY_NAME = ver
BUILD_DIR = build
EXE_NAME := ${BINARY_NAME}_$(shell go env GOHOSTOS)_$(shell go env GOHOSTARCH)
## RUN_ARGS = major minor patch
## @go install github.com/Masterminds/semver@latest

build: ## Generate production build for current OS (built under /build)
	@go get github.com/Masterminds/semver
	go build -o ./${BUILD_DIR}/${EXE_NAME} ./${TARGET_NAME}

run:
	@chmod 777 ${BUILD_DIR}/${EXE_NAME}
	@${BUILD_DIR}/${EXE_NAME}

clean:
	go clean
	rm ./${BUILD_DIR}/${EXE_NAME}
	rmdir ./${BUILD_DIR}