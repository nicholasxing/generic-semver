package main

import (
	"fmt"
	"os"
	"os/exec"
	"strings"

	"regexp"
	"github.com/Masterminds/semver"
)

func git(args ...string) string {
	git := "git"
	out, err := exec.Command(git, args...).Output()
	// DEBUG out_cmd := exec.Command(git, args...)
	// DEBUG fmt.Println(out_cmd)
	_ = err
	return string(out)
}

func tag_repo(tag string) {
	url := os.Getenv("CI_REPOSITORY_URL")

	// Transforms the repository URL to the SSH URL
	// Example input: https://gitlab-ci-token:xxxxxxxxxxxxxxxxxxxx@gitlab.com/threedotslabs/ci-examples.git
	// Example output: git@gitlab.com:threedotslabs/ci-examples.git

	url_reg := regexp.MustCompile("^(.*?)@(.*)$")
	push_url := url_reg.ReplaceAllString(url, "git@$2")

	fmt.Println(git("remote", "set-url", "--push", "origin", push_url))
	fmt.Println(git("tag", tag))
	fmt.Println(git("push", "origin", tag))
}

func bump(latest string) string {
	commit_message := strings.ToLower(os.Getenv("CI_COMMIT_MESSAGE"))
	target_branch := strings.ToLower(os.Getenv("CI_MERGE_REQUEST_TARGET_BRANCH_NAME"))
	source_branch := strings.ToLower(os.Getenv("CI_MERGE_REQUEST_SOURCE_BRANCH_NAME"))
	commit_branch := strings.ToLower(os.Getenv("CI_COMMIT_BRANCH"))

	// DEBUG commit_message := "breaking changessssss"

	ver, err := semver.NewVersion(latest)
	_ = err

	if strings.Contains(commit_message, "breaking change") {
		// bump major on breaking change
		new_ver := ver.IncMajor()
		return new_ver.String()
	} else if strings.Contains(target_branch, "release") && strings.Contains(source_branch, "feature") {
		// bump patch or minor on (release -> feature) and add/re-add prerelease
		if ver.Prerelease() == "rc" {
			new_ver, err := ver.IncPatch().SetPrerelease("rc")
			if err == nil {
				return new_ver.String()
			}
		} else {
			new_ver, err := ver.IncMinor().SetPrerelease("rc")
			if err == nil {
				return new_ver.String()
			}
		}
	} else if strings.Contains(target_branch, "main") && strings.Contains(source_branch, "development") {
		// remove prerelease on finalization
		new_ver, err := ver.SetPrerelease("")
		if err == nil {
			return new_ver.String()
		}
	} else if strings.Contains(commit_branch, "release") {
		// bump patch on committing from release
		new_ver, err := ver.IncPatch().SetPrerelease("rc")
		if err == nil {
			return new_ver.String()
		}
	} else {
		// do nothing
		new_ver, err := semver.NewVersion(latest)
		if err == nil {
			return new_ver.String()
		}
	}
	return ver.String()
}

func main() {
	version := "1.0.0"

	decode := git("describe", "--tags")
	latest := strings.TrimSuffix(string(decode), "\n")
	if latest == "" {
		// No tags in repo
		fmt.Println("No tags - initializing as 1.0.0")
	} else {
		// Skip already tagged commits
		if !strings.Contains(string(latest), "-") {
			fmt.Println(latest)
		} else {
			version = bump(latest)
		}
	}
	tag_repo(version)
	fmt.Println(version)
}
